export function API(command, method = "GET", data = {}) {
	let BaseURL = "http://localhost/public/projects/openwars/public/api/";

	return new Promise((resolve, reject) => {
		fetch(BaseURL + command, {
			method: method,
			body: JSON.stringify(data)
		})
		.then((response) => response.json())
		.then((res) => { resolve(res) })
		.catch((error) => { reject(error) });
	});
}