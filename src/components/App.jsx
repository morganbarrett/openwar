import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./home/Home";
import Game from "./game/Game";
import "semantic-ui-css/semantic.min.css";
import "./App.scss";

class App extends React.Component {
    render(){
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/game" component={Game} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;