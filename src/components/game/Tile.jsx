import React from 'react';
import "./Tile.scss";

class Tile extends React.Component {
  render(){
  	var className = "tile " + this.props.name;

  	if(this.props.full){
  		className += " full";
  	}

    return (
      <div className={className} onClick={this.props.onClick}>
        {this.props.children}
      </div>
    );
  }
}

export default Tile;