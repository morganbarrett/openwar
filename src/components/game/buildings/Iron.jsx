<div id="ironModal" class="modal">
<div class="modal-background"></div>
<div class="modal-content">
<!-- Any other Bulma elements you want -->
<div class="modal-card">
<header class="modal-card-head">
<p class="modal-card-title">Modal title</p>
<button class="delete" aria-label="close"></button>
</header>
<section class="modal-card-body">
<!-- Content ... -->
</section>
<footer class="modal-card-foot">
<button class="button is-success">Save changes</button>
<button class="button">Cancel</button>
</footer>
</div>
</div>
<button class="modal-close is-large" aria-label="close"></button>
</div>