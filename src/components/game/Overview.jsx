import _ from "lodash";
import React from "react";
import { Segment, Popup,Button } from 'semantic-ui-react';
import Tile from "./Tile";
import "./Overview.scss";

class Overview extends React.Component {
	constructor(props){
		super(props);

    	this.names = Object.keys(this.props.buildings);
		this.building = this.building.bind(this);
	}

	building(name, index){
		return (
			<li key={index} className="hexagon">
				<Tile name={name} full onClick={() => this.props.open(name)}>
					<Popup position="bottom center" content="Iron Mine" />
					
				</Tile>
			</li>
		);
	}

    render(){
        return (
        	<ul id="content">
        		{this.names.map(this.building)}
			</ul>
		);
	}
}

export default Overview;