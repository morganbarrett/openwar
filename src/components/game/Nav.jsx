import React from "react";
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
} from 'semantic-ui-react'
class Nav extends React.Component {
    render(){
        return (
 			<Menu fixed='top' stackable>
					<Container>
					<Menu.Item as='a' header>
						OPEN WAR!
					</Menu.Item>
					<Menu.Item as='a'>Home</Menu.Item>

					<Dropdown item simple text='Dropdown'>
					<Dropdown.Menu>
					<Dropdown.Item>List Item</Dropdown.Item>
					<Dropdown.Item>List Item</Dropdown.Item>
					<Dropdown.Divider />
					<Dropdown.Header>Header Item</Dropdown.Header>
					<Dropdown.Item>
					<i className='dropdown icon' />
					<span className='text'>Submenu</span>
					<Dropdown.Menu>
					<Dropdown.Item>List Item</Dropdown.Item>
					<Dropdown.Item>List Item</Dropdown.Item>
					</Dropdown.Menu>
					</Dropdown.Item>
					<Dropdown.Item>List Item</Dropdown.Item>
					</Dropdown.Menu>
					</Dropdown>

					<div className="navbar-item">
							<img src="img/icons/wood.svg" />
							242
						</div>

						<div className="navbar-item">
							<img src="img/icons/clay.svg" />
							5533
						</div>

						<div className="navbar-item">
							<img src="img/icons/iron.svg" />
							23
						</div>

						<div className="navbar-item">
							<img src="img/icons/people.svg" />
							23
						</div>
					</Container>
				</Menu>
		)
	}
}

export default Nav;