import React from "react";
import Nav from "./Nav";
import Overview from "./Overview";
import Building from "./Building";
import "./Game.scss";

class Game extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			iron: {title:"IRON MINE",open:false},
			clay: {title:"CLAY PIT",open:false},
			wood: {title:"WOOD YARD",open:false},
			storage: {title:"IRON MINE",open:false},
			farm: {title:"IRON MINE",open:false},
			church: {title:"IRON MINE",open:false},
			castle: {title:"IRON MINE",open:false},
			acadamy: {title:"IRON MINE",open:false},
			map: {title:"IRON MINE",open:false},
			barracks: {title:"IRON MINE",open:false},
			wall: {title:"IRON MINE",open:false},
			trading: {title:"IRON MINE",open:false},
			hiding: {title:"IRON MINE",open:false},
			minimize: {title:"IRON MINE",open:false}
		};

		this.names = Object.keys(this.state);

		this.building = this.building.bind(this);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
	}

	building(name, index){
 		return <Building key={index} name={name} options={this.state[name]} close={this.close} />
 	}

	open(name){
		var state = this.state;
		state[name].open = true;
		this.setState(state);
	}

	close(name){
		var state = this.state;
		state[name].open = false;
		this.setState(state);
	}

    render(){
        return (
            <div>
            	<Nav />
               	<Overview buildings={this.state} open={this.open} />
               	{this.names.map(this.building)}
			</div>
		);
	}
}

export default Game;
