import _ from 'lodash';
import React from "react";
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';
import Tile from "./Tile";
import BigText from "../common/BigText";

class Building extends React.Component {
	constructor(props){
		super(props);

		this.state = this.props.options;

		this.close = this.close.bind(this);
	}

	close(){
		this.setState({open: false});
	}

    render(){
        return (
        	<Modal open={this.state.open} onClose={this.close}>
			    <Modal.Header>
			      	<Tile name={this.props.name} />
			    	<BigText>{this.props.options.title}</BigText>
			    </Modal.Header>
			    <Modal.Content image scrolling>

			      <Modal.Description>
			        <Header>Modal Header</Header>
			        <p>This is an example of expanded content that will cause the modal's dimmer to scroll</p>

			      </Modal.Description>
			    </Modal.Content>
			    <Modal.Actions>
			      <Button color="orange">
			        Proceed <Icon name='chevron right' />
			      </Button>
			    </Modal.Actions>
			 </Modal>
		);
	}
}

export default Building;

/*
<div id="ironModal" className="modal">
					<div className="modal-background"></div>
					<div className="modal-content">
					<div className="modal-card">
					    <header className="modal-card-head">
					      <p className="modal-card-title">Modal title</p>
					      <button className="delete" aria-label="close"></button>
					    </header>
					    <section className="modal-card-body">
					    </section>
					    <footer className="modal-card-foot">
					      <button className="button is-success">Save changes</button>
					      <button className="button">Cancel</button>
					    </footer>
					  </div>
					</div>
					<button className="modal-close is-large" aria-label="close"></button>
				</div>

				<div id="popup">
					<a href="#" className="overlay"></a>

					<div className="popup iron">
						<div className="title">Iron</div>

					</div>

					<div className="popup clay">
						<div className="title">Clay</div>
					</div>

					<div className="popup wood">
						<div className="title">Wood <div>10</div></div>
						<div className="content">
							<div className="hexagon">f</div>
							<div className="hexagon big"><img width="100" height="100" src="img/icons/wood.svg" /></div>
							<div className="hexagon">f</div>

							<button className="wide">UPGRADE (Level 11)</button>
						</div>
					</div>

					<div className="popup storage">
						<div className="title">Storage</div>
					</div>

					<div className="popup farm">
						<div className="title">Farm</div>
					</div>

					<div className="popup church">
						<div className="title">Church</div>
					</div>

					<div className="popup castle">
						
					</div>

					<div className="popup acadamy">
						<div className="title">Acadamy</div>
					</div>

					<div className="popup map">
						<div className="title">Map</div>
						
					</div>

					<div className="popup barracks">
						<div className="title">Barracks</div>
					</div>

					<div className="popup wall">
						<div className="title">Wall</div>
					</div>

					<div className="popup trading">
						<div className="title">Trading</div>
					</div>

					<div className="popup hiding">
						<div className="title">Hiding</div>
					</div>

					<div className="popup profile">
						<div className="title">Morgan Barrett</div>
					</div>
				</div>
*/