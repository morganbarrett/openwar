import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, Grid, Header, Image, Message, Segment, Icon } from 'semantic-ui-react'
import BigText from "../common/BigText";
import Flow from "./common/Flow";
import Login from "./scenes/Login";

const Home = () => (
  <div className='login-form'>
    <Flow>
      <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
            <BigText size="20vmin">OPEN</BigText>
            <BigText size="22.5vmin">WAR!</BigText>
          </Header>
          <Login />
        </Grid.Column>
      </Grid>
    </Flow>
  </div>
);

export default Home;