import React from "react";
import { Segment } from 'semantic-ui-react'
import "./Flow.scss";

class Flow extends React.Component {
	render(){
		return (
			<div id="flow">
                <Segment inverted color="yellow" className="flow-1" />
                <Segment inverted color="olive" className="flow-2" />
                <Segment inverted color="green" className="flow-3" />
                <Segment className="flow-4">
                	{this.props.children}
                </Segment>
            </div>
		);
	}
}

export default Flow;