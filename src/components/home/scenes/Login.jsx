import React, {Component} from 'react';
import { Button, Form, Grid, Header, Image, Message, Segment, Icon } from 'semantic-ui-react'
import {Redirect} from 'react-router-dom';
import {API} from '../../../services/API';

class Login extends React.Component {
  constructor(){
    super();
   
    this.state = {
     username: '',
     password: '',
     redirectToReferrer: false
    };

    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  login(){
    if(this.state.username && this.state.password){
      API('user/login',"POST",this.state).then((result) => {
       let responseJson = result;
       if(responseJson.userData){         
         sessionStorage.setItem('userData',JSON.stringify(responseJson));
         this.setState({redirectToReferrer: true});
       }
      });
    }
   }

  onChange(e){
    this.setState({[e.target.name]:e.target.value});
  }

  render(){
    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/home'}/>)
    }
   
    if(sessionStorage.getItem('userData')){
      return (<Redirect to={'/home'}/>)
    }

    return (
      <Form size='large'>
        <Segment color="green">
          <Form.Input name="username" fluid icon="user" iconPosition='left' placeholder='E-mail address' onChange={this.onChange} />
          <Form.Input name="password" fluid icon='lock' iconPosition='left' placeholder='Password' type='password' onChange={this.onChange} />
          
          <Button.Group vertical fluid >
            <Button inverted color="green" onClick={this.login}>
              <Icon name='unlock' /> Login
            </Button>
            <Button color="green">
              <Icon name='signup' /> Sign Up
            </Button>
            <Button color="facebook">
              <Icon name='facebook' /> Facebook
            </Button>
          </Button.Group>
        </Segment>
      </Form>
    );
  }
}

export default Login;