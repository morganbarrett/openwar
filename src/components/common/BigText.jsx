import React from "react";
import "./BigText.scss";
import trees from "../../assets/images/trees.svg";

class BigText extends React.Component {
	render(){
		const id = "knockout-" + this.props.children;

		return (
			<div className="knockout">
				<svg className="logo" width="100%">
				 <rect className="knockout-text-bg" width="100%" height="100%" fill="white" x="0" y="0" fillOpacity="1" mask={"url(#" + id + ")"}/>
    
			    <mask id={id}>
			      <rect width="100%" height="100%" fill="#fff" x="0" y="0" />
			      <text x="50%" y="50%" fill="#000" textAnchor="middle">
				  	{this.props.children}

			      </text>
			    </mask>
				 
				</svg>
			</div>
			
		);
	}
}

export default BigText;

