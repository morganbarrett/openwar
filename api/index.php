<?php
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting( E_ALL );

	use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;
	use \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware as Whoops;

	require "../vendor/autoload.php";

	$app = new \Slim\App([
	    'settings' => [
	        'debug' => true,
        	'displayErrorDetails' => true,
        	"db" => [
        		"host" => "localhost",
        		"user" => "root",
        		"pass" => "root",
        		"dbname" => "openwar"
        	]
	    ]
	]);

	$app->add(new Whoops($app));

	$app->group('/api', function() use ($app){
		$app->group('/user', function() use ($app){
			$app->post('/login', function(Request $request, Response $response){
    			$data = $request->getParsedBody();
		        $ret = [];

		        if(isset($data["username"]) && isset($data["password"])){
		        	$username = $data["username"];
		        	$password = $data["password"];
		        	$ret = Models\User::login($username, $password);
		        } else {
		        	$ret["error"] = "Username or password not set.";
		        }

		        return json_encode($ret);
		    });

		    $app->post('/signup', function(Request $request, Response $response){
    			$data = $request->getParsedBody();
		        $ret = [];

		        if(isset($data["username"]) && isset($data["password"])){
		        	$username = $data["username"];
		        	$password = $data["password"];
		        	$ret = Models\User::login($username, $password);
		        } else {
		        	$ret["error"] = "Username or password not set.";
		        }

		        return json_encode($ret);
		    });
		});
		
		//$app->group('/customer', function() use ($app){
			//require 'routes/Customer.php';
		//});
		
		/*$app->get('/bob/{name}', function (Request $request, Response $response) {
		    $name = $request->getAttribute('name');
		    $response->getBody()->write("Hello, $name");

		    return $response;
		});*/
	});

	$app->run();