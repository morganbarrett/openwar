<?php
    namespace Models;

    class Database {
        private static $dbhost = 'localhost';
        private static $dbuser = 'root';
        private static $dbpass = 'root';
        private static $dbname = 'openwar';

        public static function create(){
            $host = self::$dbhost;
            $dbname = self::$dbname;
            $str = "mysql:host=$host;dbname=$dbname";
            $conn = new \PDO($str, self::$dbuser, self::$dbpass);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $conn;
        }
    }