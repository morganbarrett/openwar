<?php
    namespace Models;

    class User {
        public static function login($username, $password){
            try {
                $db = Database::create();
                $passwordHash = hash('sha256', $password);
                $query = $db->prepare("\
                    SELECT
                        id, username, email
                    FROM
                        users
                    WHERE (
                        username = :username OR
                        email = :username
                    ) AND (
                        password = :password
                    )
                ");
                
                $query->bindParam("username", $username, \PDO::PARAM_STR);
                $query->bindParam("password", $passwordHash, \PDO::PARAM_STR);
                $query->execute();
                
                $data = $query->fetch(\PDO::FETCH_OBJ);
               
                $db = null;

                if(!empty($data)){
                    $user_id = $data->user_id;
                    $data->token = apiToken($user_id);
                    return ["data" => $data];
                } else {
                    return ["error" => "Incorrect username or password."];
                }
            } catch(PDOException $e){
                return ["error" => $e->getMessage()];
            }
        }

        public static function signup($email, $username, $password){
            try {
                $validate = (
                    strlen(trim($email)) > 0 &&
                    strlen(trim($username)) > 0 &&
                    strlen(trim($password)) > 0 &&
                    preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email) > 0 &&
                    preg_match('~^[A-Za-z0-9_]{3,20}$~i', $username) > 0 &&
                    preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password) > 0
                );
                                
                if($validate){
                    $db = Database::create();

                    $query = $db->prepare("\
                        SELECT
                            id
                        FROM
                            users
                        WHERE (
                            username = :username OR
                            email = :email
                        )
                    ");

                    $query->bindParam("email", $email,PDO::PARAM_STR);
                    $query->bindParam("username", $username,PDO::PARAM_STR);
                    $query->execute();

                    $mainCount = $query->rowCount();
                    $created=time();

                    if($mainCount==0){
                        $passwordHash = hash('sha256',$password);

                        $stmt1 = $db->prepare("\
                            INSERT INTO 
                                users
                            (
                                email,
                                username,
                                password
                            ) VALUES (
                                :email,
                                :username,
                                :password
                            )
                        ");

                        $stmt1->bindParam("username", $username, PDO::PARAM_STR);
                        $stmt1->bindParam("password", $passwordHash, PDO::PARAM_STR);
                        $stmt1->bindParam("email", $email, PDO::PARAM_STR);
                        $stmt1->bindParam("name", $name, PDO::PARAM_STR);
                        $stmt1->execute();
                        
                        $userData = internalUserDetails($email);
                    }
                    
                    $db = null;
                 
                    if($userData){
                       return $userData;
                    } else {
                        return ["error" => "Incorrect username or password."];
                    }
                } else{
                    return ["error" => "Invalid details."];
                }
            }  catch(PDOException $e){
                return ["error" => $e->getMessage()];
            }
        }
    }